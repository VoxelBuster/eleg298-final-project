#include "Arduino.h"

#define SPKR      6
#define BTN       7
#define LED_B     10
#define LED_G     11
#define LED_R     12

#define ESP_RX    0
#define ESP_TX    1
#define ESP_EN    2
#define ESP_RST   3
#define ESP_GP0   4
#define ESP_GP2   5

#define ADXL_ZOUT 0
#define ADXL_YOUT 1
#define ADXL_XOUT 2


String IO_USERNAME = "gnare";         //paste your Adafruit IO username here
String IO_KEY = "aio_EReM73UoJuPZDHAjrC3DT4ykWfjm";    //paste your Adafruit IO key here
String WIFI_SSID = "UD Devices";      //Only need to change if using other network, eduroam won't work with ESP8266
//String WIFI_PASS = "aio_EReM73UoJuPZDHAjrC3DT4ykWfjm";                //Blank for open network
//String WIFI_SSID =  "********";         //Change to your Wi-Fi network SSID (Service Set Identifier)
String WIFI_PASS =  ""; //Change to your Wi-Fi password

void setup() {
  Serial.begin(9600);
  pinMode(SPKR, OUTPUT);
  pinMode(BTN, INPUT);
  pinMode(LED_B, OUTPUT);
  pinMode(LED_G, OUTPUT);
  pinMode(LED_R, OUTPUT);

  pinMode(ESP_EN, OUTPUT);
  pinMode(ESP_RST, OUTPUT);
  pinMode(ESP_GP0, OUTPUT);
  pinMode(ESP_GP2, OUTPUT);

  tone(SPKR, 3000, 1000);
  analogWrite(LED_B, 0);
  analogWrite(LED_G, 0);
  analogWrite(LED_R, 0);
}

void loop() {
  if (digitalRead(BTN)) {
    analogWrite(LED_B, 255);
    analogWrite(LED_G, 0);
    analogWrite(LED_R, 255);
  } else {
    analogWrite(LED_B, 255);
    analogWrite(LED_G, 255);
    analogWrite(LED_R, 0);
  }

  Serial.print(String(analogRead(ADXL_XOUT)) + " ");
  Serial.print(String(analogRead(ADXL_YOUT)) + " ");
  Serial.println(String(analogRead(ADXL_ZOUT)));

  delay(250);
}
