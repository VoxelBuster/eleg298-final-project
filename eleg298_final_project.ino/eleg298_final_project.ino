// ELEG 298 Final Project Arduino Code
// Created by Galen Nare - 5/20/2021

#include "Arduino.h"

#define SPKR      6
#define BTN       7
#define LED_B     10
#define LED_G     11
#define LED_R     12

#define ESP_RX    0
#define ESP_TX    1
#define ESP_EN    2
#define ESP_RST   3
#define ESP_GP0   4
#define ESP_GP2   5

#define ADXL_ZOUT 0
#define ADXL_YOUT 1
#define ADXL_XOUT 2


String IO_USERNAME = "gnare";         //paste your Adafruit IO username here
String IO_KEY = "aio_EReM73UoJuPZDHAjrC3DT4ykWfjm";    //paste your Adafruit IO key here
String WIFI_SSID = "UD Devices";      //Only need to change if using other network, eduroam won't work with ESP8266
//String WIFI_PASS = "aio_EReM73UoJuPZDHAjrC3DT4ykWfjm";                //Blank for open network
//String WIFI_SSID =  "********";         //Change to your Wi-Fi network SSID (Service Set Identifier)
String WIFI_PASS =  ""; //Change to your Wi-Fi password

volatile uint8_t led_r = 255, led_g = 255, led_b = 255;
volatile uint8_t led_mode = 0;

void setup() {
  Serial.begin(9600);
  pinMode(SPKR, OUTPUT);
  pinMode(BTN, INPUT);
  pinMode(LED_B, OUTPUT);
  pinMode(LED_G, OUTPUT);
  pinMode(LED_R, OUTPUT);

  pinMode(ESP_EN, OUTPUT);
  pinMode(ESP_RST, OUTPUT);
  pinMode(ESP_GP0, OUTPUT);
  pinMode(ESP_GP2, OUTPUT);

  tone(SPKR, 3000, 250); // Startup beep
  analogWrite(LED_B, led_b);
  analogWrite(LED_G, led_g);
  analogWrite(LED_R, led_r);

  digitalWrite(ESP_GP0, HIGH); // Boot from ESP01 Flash
  digitalWrite(ESP_GP2, LOW);
  digitalWrite(ESP_EN, HIGH);

  digitalWrite(ESP_RST, LOW); // Start the ESP01
  delay(100);
  digitalWrite(ESP_RST, HIGH);

  // Set up timer 1 interrupt for blink setup indicator
  led_mode = 1;
  startTimer1();

  String resp = espData("get_macaddr",2000,false);  //get MAC address of 8266
  resp = espData("wifi_ssid="+WIFI_SSID,2000,false);  //send Wi-Fi SSID to connect to
  Serial.println(resp);
  resp = espData("wifi_pass="+WIFI_PASS,2000,false);  //send password for Wi-Fi network
  Serial.println(resp);
  resp = espData("io_user="+IO_USERNAME,2000,false);  //send Adafruit IO info
  Serial.println(resp);
  resp = espData("io_key="+IO_KEY,2000,false);
  Serial.println(resp);
  resp = espData("setup_io",15000,true);      //setup the IoT connection
  Serial.println(resp);
  if(resp.indexOf("connected") < 0) {
    stopTimer1();
    led_mode = 2;
    led_r = 0;
    led_g = 255;
    led_b = 255;
    analogWrite(LED_B, led_b);
    analogWrite(LED_G, led_g);
    analogWrite(LED_R, led_r);
    return;
  }
  resp = espData("setup_feed=1,Shock-sensor-alert",2000,false);  //start the data feed
  resp = espData("setup_feed=2,Shock-sensor-X",2000,false);
  resp = espData("setup_feed=3,Shock-sensor-Y",2000,false);
  resp = espData("setup_feed=4,Shock-sensor-Z",2000,false);
  stopTimer1();
  led_mode = 3;
  led_r = 255;
  led_g = 0;
  led_b = 255;
  analogWrite(LED_B, led_b);
  analogWrite(LED_G, led_g);
  analogWrite(LED_R, led_r);
}

ISR(TIMER1_COMPA_vect){  // Timer 1 ISR - Status LED
   if (led_mode == 1) {
     if (led_r == 255 && led_g == 255) {
       led_r = 0;
       led_g = 0;
       led_b = 255;
     } else {
       led_r = 255;
       led_g = 255;
       led_b = 255;
     }
   } else if (led_mode == 9) {
     if (led_b == 255 && led_g == 255) {
       tone(SPKR, 4000);
       led_r = 255;
       led_g = 255;
       led_b = 0;
     } else {
       tone(SPKR, 500);
       led_r = 0;
       led_g = 255;
       led_b = 255;
     }
   } else {
     led_r = 255;
     led_g = 255;
     led_b = 255;
   }
   
   analogWrite(LED_B, led_b);
   analogWrite(LED_G, led_g);
   analogWrite(LED_R, led_r);
}

void startTimer1() {
  cli();

  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 1hz increments
  OCR1A = 3906;// = (4*10^6) / (1*1024) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS10 and CS12 bits for 1024 prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10);  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  sei();
}

void stopTimer1() {
  cli();
  TIMSK1 &= ~(1 << OCIE1A); // turn off the timer interrupt
  sei();
}

String espData(String command, const int timeout, boolean debug) {
  String response = "";
  Serial.println(command); //send data to ESP8266 using serial UART
  long int time = millis();
  while ( (time + timeout) > millis()) {  //wait the timeout period sent with the command
    while (Serial.available()) { //look for response from ESP8266
      char c = Serial.read();
      response += c;
      //Serial.print(c);  //print response on serial monitor
    }
  }
  response.trim();
  return response;
}

int x_old = -1, y_old = -1, z_old = -1;

void alarm() {
  startTimer1();
  while (1) {
    String resp = espData("send_data=1,1",2000,false);
  }
}

int wait = 3;

void loop() {
  int x = analogRead(ADXL_XOUT);
  Serial.print(x);
  Serial.print(" ");
  int y = analogRead(ADXL_YOUT);
  Serial.print(y);
  Serial.print(" ");
  int z = analogRead(ADXL_ZOUT);
  Serial.println(z);

  String resp = espData("send_data=1,0",150,false);
  resp = espData("send_data=2,"+String(x),150,false);
  resp = espData("send_data=3,"+String(y),150,false);
  resp = espData("send_data=4,"+String(z),150,false);

  if (wait <= 0) {
    if ((float) (abs(x - x_old) / x) > 0.2 && x_old != -1) {
      led_mode = 9;
      alarm();
    }
    if ((float) (abs(y - y_old) / y) > 0.2 && y_old != -1) {
      led_mode = 9;
      alarm();
    }
    if ((float) (abs(z - z_old) / z) > 0.2 && z_old != -1) {
      led_mode = 9;
      alarm();
    }
  } else {
    wait -= 1;
  }

  x_old = x;
  y_old = y;
  z_old = z;
}
